package com.example.android.quakereport;

/**
 * Created by Mayank Mrinal Tiwary on 10-11-2017.
 */

public class EarthQuake {
    String location;
    String time;
    String mag;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMag() {
        return mag;
    }

    public void setMag(String mag) {
        this.mag = mag;
    }

    public EarthQuake(String time, String mag, String location){
        this.location=location;
        this.time=time;
        this.mag=mag;


    }
}

