/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.quakereport;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.os.AsyncTask;
import android.util.Log;

import com.example.android.quakereport.driver.HttpHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class EarthquakeActivity extends AppCompatActivity {

    public static final String LOG_TAG = EarthquakeActivity.class.getName();
    ArrayList<EarthQuake> earthquakes = new ArrayList<>();
    ListView earthquakeListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.earthquake_activity);

        // Create list of earthquake locations.

        new QueryUtils().execute();


        // Find a reference to the {@link ListView} in the layout

    }

    public  class QueryUtils extends AsyncTask<Void, Void, Void>{

        /** Sample JSON response for a USGS query */

        private String TAG = QueryUtils.class.getSimpleName();

        public QueryUtils() {
        }

        /**
         * Return a list of {@link EarthQuake} objects that has been built up from
         * parsing a JSON response.
         */

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();
            // Making a request to url and getting response
            String url = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/4.5_week.geojson";

            //query url
//        String url = "https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2016-01-28&endtime=2016-01-31&minmag=6&limit=10";
            String jsonStr = sh.makeServiceCall(url);

            Log.e(TAG, "Response from url: " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject root = new JSONObject(jsonStr);
                    JSONArray features = root.optJSONArray("features");
                    for (int i =0; i<10; i++) {

                        JSONObject quakeobject = features.getJSONObject(i);
                        JSONObject properties = quakeobject.getJSONObject("properties");
                        String time = (properties.getString("time"));
                        String mag = (properties.getString("mag"));
                        String location = properties.getString("place");
                        earthquakes.add(new EarthQuake(time, mag, location));
                    }


                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                }

            } else {
                Log.e(TAG, "Couldn't get json from server.");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            earthquakeListView = (ListView) findViewById(R.id.list);

            // Create a new {@link ArrayAdapter} of earthquakes
            MyAdapter adapter = new MyAdapter(
                    EarthquakeActivity.this, android.R.layout.simple_list_item_1, earthquakes);

            // Set the adapter on the {@link ListView}
            // so the list can be populated in the user interface
            earthquakeListView.setAdapter(adapter);

        }


    }

}
