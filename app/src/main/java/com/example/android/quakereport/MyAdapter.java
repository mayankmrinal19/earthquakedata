package com.example.android.quakereport;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Mayank Mrinal Tiwary on 13-11-2017.
 */

public class MyAdapter extends ArrayAdapter<EarthQuake> {

    ArrayList<EarthQuake> quakeList = new ArrayList<>();
    public MyAdapter(Context context, int textViewResourceId, ArrayList<EarthQuake> objects) {
        super(context, textViewResourceId, objects);
        quakeList = objects;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.list_item, null);
        TextView mag = (TextView) v.findViewById(R.id.mag);
        TextView time = (TextView) v.findViewById(R.id.time);
        TextView loc = (TextView)v.findViewById(R.id.loc);
        mag.setText(quakeList.get(position).getMag());
        time.setText(quakeList.get(position).getTime());
        loc.setText(quakeList.get(position).getLocation());
        return v;

    }


}
